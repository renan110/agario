## Commandes

npm run build:server 
npm run build
npm start

## Sommaire
- [A. Diagramme de séquence](#a-Diagramme-de-séquence)
- [B. Difficultés techniques](#b-Difficultés-techniques)
- [C. Points d'amélioration/d'achèvement](#c-Points-d'amélioration/d'achèvement)
- [D. Ce dont je suis le plus fier](#d-Ce-dont-je-suis-le-plus-fier)


## A. Diagramme de séquence
<img src="./client/public/images/readme/diagramme_de_séquence.png">

## B. Difficultés techniques
Lors du passage d'un objet de client a serveur et inversement, perte des fonctions qui lui sont associée.

--> Réaffectation des données dans des objets locaux.
## C. Points d'amélioration/d'achèvement
- Ajouter la partie leaderboard.

- Améliorer la qualité visuelle globale.

- Continuer l'optimisation.

- Utilisation du Gyroscope sur smartphone.

- Ajouter des messages a tous les joueurs en cas de mort ou d'arrivée sur la partie d'un joueur.

## D. Ce dont je suis le plus fier
- Choix des skins 
