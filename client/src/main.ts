import Mouse from '../../common/mouse';
import Map from '../../common/Map';
import Food from '../../common/Food';
import Player, { displayAllPlayers } from '../../common/Player';
import { displayAllFood } from '../../common/Food';
import {
	lauchGame,
	backToMenu,
	back,
	showLeaderboard,
	player,
	onDeath,
	showCredits,
} from './StartGame';
import {
	changePatternRight,
	changePatternLeft,
	changeColorRight,
	changeColorLeft,
} from './Caroussel';
import { io } from 'socket.io-client';
export const socket = io();

const canvas = document.querySelector('.gameCanvas') as HTMLCanvasElement;
const context = canvas.getContext('2d') as CanvasRenderingContext2D;
const mouse = new Mouse(canvas);
canvas.height = window.innerHeight;
canvas.width = window.innerWidth;
const screenWidth: number = canvas.width;
const screenHeight: number = canvas.height;

let poids: number;
const currentPoids = document.querySelector('.poids') as HTMLInputElement;

let map: Map = new Map(500, 500);
let players: Player[] = [];
let foods: Food[] = [];
let id: string;

export function render() {
	map.clearDisplay(context, map.height, map.width); // On clear le fond
	context.save(); // Save l'état actuel
	player.translate(context); // Affiche la bonne vue
	map.drawMap(context); // Affiche la map
	displayAllFood(player, foods, screenWidth, screenHeight, context); // Affiche la nourriture
	displayAllPlayers(players, context); // Affiche les Joueurs
	context.restore(); // Restore la vue initiale
	requestAnimationFrame(render);
}
render();

function update() {
	socket.emit('movePlayerInfo', [mouse, screenHeight, screenWidth]);
	poids = Math.round((player.rayon * player.rayon * Math.PI) / 100);
	currentPoids.innerHTML = String(poids) + ' KG';
}
setInterval(update, 1000 / 60);

socket.on('setMap', mapSet => {
	//Récupere la map envoyé par le serveur lors de la connection
	map.width = mapSet.width;
	map.height = mapSet.height;
});

socket.on('getId', playerId => {
	//Récupere la map envoyé par le serveur lors de la connection
	id = playerId;
});

socket.on('setFoods', setFood => {
	//Ajoute toutes les nourriture pour initialiser la liste de nourriture de départ
	foods.length = 0;
	for (let i = 0; i < setFood.length; i++) {
		let food: Food = new Food(
			setFood[i].id,
			setFood[i].x,
			setFood[i].y,
			setFood[i].rayon,
			setFood[i].color
		);
		foods.push(food);
	}
});

socket.on('foodDelete', idFoodDelete => {
	// Supprime une nourriture de la liste
	for (let i = 0; i < foods.length; i++) {
		if (foods[i].id === idFoodDelete) {
			foods = foods.filter(food => food !== foods[i]);
		}
	}
});

socket.on('foodAdd', foodToAdd => {
	//Ajoute une nourriture dans la liste
	let food: Food = new Food(
		foodToAdd.id,
		foodToAdd.x,
		foodToAdd.y,
		foodToAdd.rayon,
		foodToAdd.color
	);
	foods.push(food);
});

socket.on('thisPlayer', thisPlayer => {
	// Récupere les informations de son propre joueur
	player.id = thisPlayer.id;
	player.x = thisPlayer.x;
	player.y = thisPlayer.y;
	player.rayon = thisPlayer.rayon;
});

socket.on('playerList', playerList => {
	//Récupere les informations de tous les joueurs
	players.length = 0;
	for (let i = 0; i < playerList.length; i++) {
		let player: Player = new Player(
			playerList[i].id,
			playerList[i].x,
			playerList[i].y,
			playerList[i].rayon,
			playerList[i].skin,
			playerList[i].name
		);

		players.push(player);
	}
});

socket.on('someoneIsDead', idName => {
	//Si quelqu'un est mort
	const playerId: string = idName[0];
	const playerName: string = idName[1];
	let poids: number = Math.round(idName[2] / 100);
	console.log('Le joueur ', playerName, ' est mort !');
	if (playerId === id) {
		//Si c'est nous qui sommes mort,
		onDeath(poids); //On lance la fonction onDeath qui termine la partie et lance l'écran gameOver(rejouer)
	}
});

//Button menu
const rightName = document.querySelector(
		'.skin .arrow-right'
	) as HTMLInputElement,
	leftName = document.querySelector('.skin .arrow-left') as HTMLInputElement,
	rightColor = document.querySelector(
		'.color .arrow-right'
	) as HTMLInputElement,
	leftColor = document.querySelector('.color .arrow-left') as HTMLInputElement;

rightName.addEventListener('click', changePatternRight);
leftName.addEventListener('click', changePatternLeft);
rightColor.addEventListener('click', changeColorRight);
leftColor.addEventListener('click', changeColorLeft);

const playBtn = document.querySelector('.bottom button') as HTMLInputElement;
playBtn.addEventListener('click', lauchGame);

//Button gameover
const playAgainBtn = document.querySelector(
	'.gameover .bottom button'
) as HTMLInputElement;
playAgainBtn.addEventListener('click', lauchGame);

const BackMenuBtn = document.querySelector(
	'.gameover .bottom button:nth-child(2)'
) as HTMLInputElement;
BackMenuBtn.addEventListener('click', backToMenu);

const leaderboardBtn = document.querySelector(
	'.gameover .option button:nth-child(1)'
) as HTMLInputElement;
leaderboardBtn.addEventListener('click', showLeaderboard);

const creditsBtn = document.querySelector(
	'.gameover .option button:nth-child(2)'
) as HTMLInputElement;
creditsBtn.addEventListener('click', showCredits);

//Button leaderboard
const BackBtnLeaderboard = document.querySelector(
	'.leaderboard .bottom button:nth-child(1)'
) as HTMLInputElement;
BackBtnLeaderboard.addEventListener('click', back);

const BackMenuBtn2 = document.querySelector(
	'.leaderboard .bottom button:nth-child(2)'
) as HTMLInputElement;
BackMenuBtn2.addEventListener('click', backToMenu);

const BackMenuBtnCredits = document.querySelector(
	'.credits .bottom button:nth-child(2)'
) as HTMLInputElement;
BackMenuBtnCredits.addEventListener('click', backToMenu);

const BackBtnCredits = document.querySelector(
	'.credits .bottom button:nth-child(1)'
) as HTMLInputElement;
BackBtnCredits.addEventListener('click', back);
