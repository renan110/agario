import Player from '../../common/Player';
import { pattern, color } from './Caroussel';
import { socket } from './main';

const finalPoids = document.querySelector(
	'.gameover .divide .info p:nth-child(1)'
) as HTMLInputElement;

const timeAlive = document.querySelector(
	'.gameover .divide .info p:nth-child(2)'
) as HTMLInputElement;
const currentTime = document.querySelector('.timer') as HTMLInputElement;
const finalScore = document.querySelector(
	'.gameover .divide .info p:nth-child(3)'
) as HTMLInputElement;

let sec: number = 0;
let min: number = 0;
let hrs: number = 0;
let game: string = 'no';

export let player: Player = new Player(
	'1',
	2000,
	2000,
	50,
	'../images/skin/default.png',
	'salut'
);
const menu = document.querySelector('.menu') as HTMLInputElement,
	gameover = document.querySelector('.gameover') as HTMLInputElement,
	leaderboard = document.querySelector('.leaderboard') as HTMLInputElement,
	credits = document.querySelector('.credits') as HTMLInputElement,
	inGameInfo = document.querySelector('.inGameInfo') as HTMLInputElement,
	playerNameInput = document.querySelector(
		'input[name=name]'
	) as HTMLInputElement;
let playerSkin: string = '';

const music = new Audio('./sounds/inGameMusic.mp3');

export function lauchGame() {
	const playerName: string = playerNameInput.value;
	let data: String[] = [];
	if (playerName === '') {
		alert('le champ "Nom de Joueur" est obligatoire');
		return;
	} else {
		if (pattern.innerHTML === 'pattern' || color.innerHTML === 'color') {
			//Si pas de skin choisi
			menu.style.display = 'none';
			gameover.style.display = 'none';
			inGameInfo.style.display = 'flex';
			playerSkin = '../images/skin/default.png';
			data = [playerName, playerSkin];
			socket.emit('playerInfo', data); //Envoie les informations nécessaire a la création du joueur pour lancer la partie
			sec = 0; //Initialise le timer a 0
			min = 0;
			hrs = 0;
			game = 'yes';
			music.play(); //Lance la musique au début du jeu
			inGameInfo.style.display = 'flex'; //Affichage de la fenetre indicative de poids et de temps pendant le jeu
		} else {
			//Si un skin est choisi
			menu.style.display = 'none';
			gameover.style.display = 'none';
			playerSkin =
				'../images/skin/' + pattern.innerHTML + '-' + color.innerHTML + '.png';
			data = [playerName, playerSkin];
			socket.emit('playerInfo', data); //Envoie les informations nécessaire a la création du joueur pour lancer la partie
			game = 'yes';
			music.play(); //Lance la musique au début du jeu
			inGameInfo.style.display = 'flex'; //Affichage de la fenetre indicative de poids et de temps pendant le jeu
		}
	}
}

export function backToMenu() {
	gameover.style.display = 'none';
	leaderboard.style.display = 'none';
	credits.style.display = 'none';
	inGameInfo.style.display = 'none';
	menu.style.display = 'flex';
}

export function back() {
	leaderboard.style.display = 'none';
	credits.style.display = 'none';
	gameover.style.display = 'flex';
}

export function showLeaderboard() {
	gameover.style.display = 'none';
	leaderboard.style.display = 'flex';
}

export function showCredits() {
	gameover.style.display = 'none';
	credits.style.display = 'flex';
}

export function onDeath(poids: number) {
	gameover.style.display = 'flex'; //On affiche l'écran gameover
	game = 'no'; //On stop la partie (le timer)
	finalPoids.innerHTML = 'Poids: ' + String(poids) + ' KG'; //On affiche son poids final
	timeAlive.innerHTML =
		'Time: ' + String(hrs) + 'h : ' + String(min) + 'm : ' + String(sec) + 's'; //On affiche son tems de survie
	let score = Math.round(poids + (200 / 60) * (sec + 60 * min + 3600 * hrs)); //On calcule son score en fonction des 2 paramétres précédents
	finalScore.innerHTML = 'Score: ' + String(score); //On affiche son score final
	inGameInfo.style.display = 'none'; //On enleve la fenetre indicative affichée pendant le jeu
}

setInterval(tick, 1000);

function tick() {
	if (game === 'yes') {
		sec++;
		if (sec >= 60) {
			sec = 0;
			min++;
			if (min >= 60) {
				min = 0;
				hrs++;
			}
		}
		currentTime.innerHTML =
			String(hrs) + 'h : ' + String(min) + 'm : ' + String(sec) + 's';
	}
}
