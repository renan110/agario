const skinPattern = ['fire', 'space', 'bird', 'iris', 'fleur'];
const skinColor = ['blue', 'purple', 'green', 'yellow'];

const preview = document.querySelector('.preview') as HTMLInputElement;
export const pattern: HTMLInputElement = document.querySelector(
	'.skin .text-choice p'
) as HTMLInputElement;
export const color: HTMLInputElement = document.querySelector(
	'.color .text-choice p'
) as HTMLInputElement;
const bgDefault: HTMLInputElement = document.querySelector(
	'.color .text-choice'
) as HTMLInputElement;

export function changePatternRight(): void {
	if (skinPattern.indexOf(pattern.innerHTML) <= skinPattern.length - 2) {
		pattern.innerHTML = skinPattern[skinPattern.indexOf(pattern.innerHTML) + 1];
	} else {
		pattern.innerHTML = skinPattern[0];
	}
	displayPreview(pattern, color);
}

export function changePatternLeft(): void {
	if (skinPattern.indexOf(pattern.innerHTML) > 0) {
		pattern.innerHTML = skinPattern[skinPattern.indexOf(pattern.innerHTML) - 1];
	} else {
		pattern.innerHTML = skinPattern[skinPattern.length - 1];
	}
	displayPreview(pattern, color);
}

export function changeColorRight(): void {
	if (skinColor.indexOf(color.innerHTML) <= skinColor.length - 2) {
		color.innerHTML = skinColor[skinColor.indexOf(color.innerHTML) + 1];
	} else {
		color.innerHTML = skinColor[0];
	}
	displayPreview(pattern, color);
	bgDefault.style.backgroundColor = color.innerHTML;
}

export function changeColorLeft(): void {
	if (skinColor.indexOf(color.innerHTML) > 0) {
		color.innerHTML = skinColor[skinColor.indexOf(color.innerHTML) - 1];
	} else {
		color.innerHTML = skinColor[skinColor.length - 1];
	}
	displayPreview(pattern, color);

	bgDefault.style.backgroundColor = color.innerHTML;
}

function displayPreview(
	pattern: HTMLInputElement,
	color: HTMLInputElement
): void {
	if (color.innerHTML === 'color') {
		preview.style.backgroundImage =
			'url(../images/' + pattern.innerHTML + '.png)';
	} else if (pattern.innerHTML === 'pattern') {
		preview.style.backgroundImage =
			'url(../images/' + color.innerHTML + '.png)';
	} else {
		preview.style.backgroundImage =
			'url(../images/skin/' +
			pattern.innerHTML +
			'-' +
			color.innerHTML +
			'.png)';
	}
}
