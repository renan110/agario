import { tsServerOk } from '../src/lib';
import { createFood, checkFoodCollider } from '../../common/Food';
import Map from '../../common/Map';
import Food, { createAllFoods } from '../../common/Food';
import Player, { checkPlayerCollider, playerIsDead } from '../../common/Player';

describe('tsServerOK', () => {
	it('should return OK', function () {
		expect(tsServerOk()).toBe('Ok');
	});
});

let map: Map = new Map(900, 900);
describe('Food', () => {
	it('should add food', function () {
		let listFood: Food[] = [];
		expect(listFood.length).toBe(0);
		createFood(listFood, map);
		expect(listFood.length).toBe(1);
	});

	it('should instanciate all food at start', function () {
		let listFood: Food[] = [];
		//map is 900*900, max food should be 900/300 * 900/300 = 9 foods
		createAllFoods(listFood, map);
		expect(listFood.length).toBe(9);
	});

	it('should return foodId if collision', function () {
		let listFood: Food[] = [];
		let food: Food = createFood(listFood, map);
		//listFood contient maintenant 1 food, on va créer un player qui ne le touche pas
		let player = new Player(
			'id',
			food.x + 100,
			food.y + 100,
			50,
			'default.png',
			'test'
		);
		//Aucun éléments en collision
		expect(checkFoodCollider(player, listFood)).toBe(0);

		//On bouge le joueur pour qu'il touche le food
		player.x = food.x + 10;
		player.y = food.y + 10;
		//le food est touché, cela devrait retourner son id
		expect(checkFoodCollider(player, listFood)).toBe(listFood[0].id);
	});
});

describe('Player', () => {
	it('should return no collision between players', function () {
		//on créé une liste contenant 2 joueurs qui ne se touchent pas
		let player1 = new Player('id', 100, 100, 50, 'default.png', 'test');
		let player2 = new Player('id', 400, 400, 100, 'default.png', 'test');
		let listPlayer: Player[] = [player1, player2];
		expect(checkPlayerCollider(listPlayer)).toBe('0');
	});

	it('should return no collision because similar size', function () {
		//on créé une liste contenant 2 joueurs qui se touche mais on une taille similaire
		let player1 = new Player('id', 400, 400, 102, 'default.png', 'test');
		let player2 = new Player('id', 400, 400, 100, 'default.png', 'test');
		let listPlayer: Player[] = [player1, player2];
		expect(checkPlayerCollider(listPlayer)).toBe('0');
	});

	it('should return dead player id + name (checkPlayerCollider)', function () {
		//on créé une liste contenant 2 joueurs qui se mangent
		let player1 = new Player('id1', 350, 350, 50, 'default.png', 'test');
		let player2 = new Player('id2', 400, 400, 150, 'default.png', 'test');
		let listPlayer: Player[] = [player1, player2];
		expect(checkPlayerCollider(listPlayer)).toStrictEqual([
			player1.id,
			player1.name,
		]);
	});

	it('should remove dead player from listPlayer (playerIsDead)', function () {
		//on créé une liste contenant 2 joueurs qui se mangent
		let player1 = new Player('id1', 350, 350, 50, 'default.png', 'test');
		let player2 = new Player('id2', 400, 400, 150, 'default.png', 'test');
		let listPlayer: Player[] = [player1, player2];
		let listPlayerExpected: Player[] = [player2];
		expect(playerIsDead(listPlayer, player1.id)).toStrictEqual(
			listPlayerExpected
		);
	});
});
