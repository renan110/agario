import express from 'express';
import http from 'http';
import { Server as IOServer } from 'socket.io';
import addWebpackMiddleware from './addWebpackMiddleware.js';
import Player, {
	checkPlayerCollider,
	playerIsDead,
	setFinalPoids,
} from '../../common/Player';
import Food, {
	checkFoodCollider,
	createAllFoods,
	createFood,
} from '../../common/Food.js';
import Map from '../../common/Map';

const app = express(),
	httpServer = http.createServer(app);

addWebpackMiddleware(app); // compilation js client
app.use(express.static('client/public')); // fichiers statiques (html, css, js)

const io = new IOServer(httpServer);
let playersList: Player[] = [];
let foodsList: Food[] = [];
const map: Map = new Map(20000, 20000);
createAllFoods(foodsList, map); //Crée la nourriture
let finalPoids: number;

// connexion d'un nouveau client au serveur websocket
// (déclenché par la ligne `const socket = io();` du client)
io.on('connection', socket => {
	console.log(`Nouvelle connexion du client ${socket.id}`);

	socket.emit('setMap', map); //Envoie la map au client lors de sa connection
	socket.emit('setFoods', foodsList); //Envoie la liste de nourriture au client lors de sa connection
	socket.emit('getId', socket.id); // Envoie au client concerné son identifiant

	socket.on('playerInfo', data => {
		// Créer le joueur lors de la requete du client
		const playerName: string = data[0];
		const playerSkin: string = data[1];
		let player: Player = new Player(
			socket.id,
			50 + Math.round(Math.random() * (map.height - 2 * 50)),
			50 + Math.round(Math.random() * (map.width - 2 * 50)),
			53,
			playerSkin,
			playerName
		);
		playersList.push(player);
		io.emit('playerList', playersList); //Envoie la liste mise a jour a tous les joueurs
	});

	socket.on('movePlayerInfo', data => {
		//Récupere les informations envoyées par le client
		let mouse = data[0], //Position de la souris
			screenHeight = data[1], //hauteur d'écran
			screenWidth = data[2]; //largeur d'écran
		for (let i = 0; i < playersList.length; i++) {
			if (playersList[i].id === socket.id) {
				//Si c'est le bon joueur
				playersList[i] = playersList[i].movePlayer(
					//Effectue les déplacement sur le joueur
					mouse,
					screenHeight,
					screenWidth,
					map
				);
				socket.emit('thisPlayer', playersList[i]); //envoie au client son joueur
				io.emit('playerList', playersList); //envoyer a tous le monde la liste mise a jour
			}
		}
	});

	function update() {
		//Vérification de collision entre un joueur et de la nourriture
		for (let i = 0; i < playersList.length; i++) {
			let idToDestroy: number = checkFoodCollider(playersList[i], foodsList);
			if (idToDestroy !== 0) {
				io.emit('foodDelete', idToDestroy); //Envoie l'id de la nourriture a détruire
				foodsList = foodsList.filter(food => food.id !== idToDestroy);
				let foodToAdd: Food = createFood(foodsList, map);
				io.emit('foodAdd', foodToAdd); //Envoie la nourriture a ajouter
			}
		}

		//Vérification de collision entre deux joueurs
		let idDead: string = '0';
		let playerName: string;
		[idDead, playerName] = checkPlayerCollider(playersList);
		if (idDead === '0') {
			//si personne n'est mort, rien
		} else {
			//Si une personne est morte
			finalPoids = setFinalPoids(playersList, idDead);
			io.emit('someoneIsDead', [idDead, playerName, finalPoids]); //On envoie a tous le monde ses informations
			playersList = playerIsDead(playersList, idDead);
			idDead = '0';
		}
	}
	setInterval(update, 1000 / 30);

	// déconnexion
	socket.on('disconnect', () => {
		playersList = playersList.filter(player => player.id !== socket.id);
		console.log(`Déconnexion du client ${socket.id}`);
	});
});

const port = process.env.PORT || 8000;
httpServer.listen(port, () => {
	console.log(`Server running at http://localhost:${port}/`);
});
