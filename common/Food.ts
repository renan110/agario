import Player from './Player';
import Map from './Map';

export default class Food {
	id: number;
	x: number;
	y: number;
	rayon: number;
	color: string;
	constructor(id: number, x: number, y: number, rayon: number, color: string) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.rayon = rayon;
		this.color = color;
	}
}

function displayFood(context: CanvasRenderingContext2D, food: Food) {
	//Affiche une nourriture
	context.beginPath();
	context.arc(food.x, food.y, food.rayon, 0, 2 * Math.PI, false);
	context.fillStyle = food.color;
	context.fill();
	context.stroke();
}

export function displayAllFood( //boucle d'affichages de toutes les nourritures a proximité
	player: Player,
	liste: Food[],
	screenWidth: number,
	screenHeight: number,
	context: CanvasRenderingContext2D
) {
	for (let printFood = 0; printFood < liste.length; printFood++) {
		if (player.rayon < 375) {
			if (
				Math.abs(liste[printFood].x - player.x) < screenWidth &&
				Math.abs(liste[printFood].y - player.y) < screenHeight
			) {
				displayFood(context, liste[printFood]);
			}
		} else if (player.rayon < 750) {
			if (
				Math.abs(liste[printFood].x - player.x) < 2 * screenWidth &&
				Math.abs(liste[printFood].y - player.y) < 2 * screenHeight
			) {
				displayFood(context, liste[printFood]);
			}
		} else {
			if (
				Math.abs(liste[printFood].x - player.x) < 3 * screenWidth &&
				Math.abs(liste[printFood].y - player.y) < 3 * screenHeight
			) {
				displayFood(context, liste[printFood]);
			}
		}
	}
}

let colorList: string[] = [
	'blue',
	'green',
	'red',
	'purple',
	'yellow',
	'orange',
];
export function createAllFoods(liste: Food[], map: Map) {
	//Initialise la liste de nourriture commune
	let nbFood: number = liste.length;
	const rayon: number = 10;

	//calcul pour avoir un nombre correct de nourriture
	const foodRate: number = 300; //Proportion de nourriture sur la map
	while (nbFood < (map.width / foodRate) * (map.height / foodRate)) {
		let randomColor: string =
			colorList[Math.round(Math.random() * (colorList.length - 1))]; //séléctionne une couleur aléatoire parmis celles de la liste
		let food = new Food(
			Math.round(Math.random() * 10000000), //Identifiant de nourriture aléatoire / pas d'impact si 2 fois le meme id
			rayon + Math.round(Math.random() * (map.height - 2 * rayon)), //disposition aléatoire en x
			rayon + Math.round(Math.random() * (map.width - 2 * rayon)), //disposition aléatoire en y
			rayon,
			randomColor
		);
		liste.push(food);
		nbFood += 1;
	}
}

export function createFood(liste: Food[], map: Map) {
	const rayon: number = 10;
	let random: string =
		colorList[Math.round(Math.random() * colorList.length - 1)];
	let food = new Food(
		Math.round(Math.random() * 10000000),
		rayon + Math.round(Math.random() * (map.height - 2 * rayon)),
		rayon + Math.round(Math.random() * (map.width - 2 * rayon)),
		rayon,
		random
	);
	liste.push(food);
	return food;
}

export function checkFoodCollider(player: Player, liste: Food[]) {
	//Regarde si il y a une collision entre un joueur et une nourriture
	for (let i = liste.length - 1; i >= 0; i--) {
		const distance = Math.hypot(player.x - liste[i].x, player.y - liste[i].y); //On calcule la distance entre les objets
		//on regarde si la distance est inferieur a la somme des rayons pour savoir si ils se touchent
		if (liste[i].rayon + player.rayon > distance) {
			//Si ils se touchent:
			let airTotale =
				player.rayon * player.rayon * Math.PI +
				liste[i].rayon * liste[i].rayon * Math.PI; //On additionne leurs air
			player.rayon = Math.sqrt(airTotale / Math.PI); //On affecte au joueur sa nouvelle masse
			return liste[i].id;
		}
	}
	return 0;
}
