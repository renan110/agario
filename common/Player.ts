import Map from './Map';
import Mouse from './mouse';

export default class Player {
	id: string;
	x: number;
	y: number;
	rayon: number;
	speed: number;
	distanceMousePlayerX: number;
	distanceMousePlayerY: number;
	angle: number;
	skin: string;
	name: string;

	constructor(
		id: string,
		x: number,
		y: number,
		rayon: number,
		skin: string,
		name: string
	) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.rayon = rayon;
		this.speed = 10;
		this.distanceMousePlayerX = 0;
		this.distanceMousePlayerY = 0;
		this.angle = 0;
		this.skin = skin;
		this.name = name;
	}

	//
	translate(context: CanvasRenderingContext2D) {
		//Axe d'amélioration : trouver une formule mathématique pour dézoomer
		let spaceView: number;
		if (this.rayon <= 100) {
			spaceView = 1;
		} else if (this.rayon < 400) {
			//Si le rayon <400
			spaceView = 1 - (this.rayon - 100) / 600; //Alors on dézoom un peu
		} else if (this.rayon < 500) {
			spaceView = 1 - this.rayon / 800;
		} else if (this.rayon < 600) {
			spaceView = 1 - (this.rayon + 500) / 1600;
		} else if (this.rayon < 800) {
			spaceView = 1 - (this.rayon + 900) / 2205;
		} else if (this.rayon < 1100) {
			spaceView = 1 - (this.rayon + 2200) / 3900;
		} else {
			spaceView = 1 / 6;
		}

		let spaceTranslate: number = 1 / spaceView;
		context.scale(spaceView, spaceView);
		context.translate(
			//On met l'affichage sur le joueur pour qu'il sois au center
			(spaceTranslate * window.innerWidth) / 2 - this.x,
			(spaceTranslate * window.innerHeight) / 2 - this.y
		);
	}

	//Affiche un joueur
	displayPlayer(context: CanvasRenderingContext2D) {
		const image: HTMLImageElement = new Image();
		image.src = this.skin;
		context.beginPath();
		context.drawImage(
			image,
			this.x - this.rayon,
			this.y - this.rayon,
			this.rayon * 2,
			this.rayon * 2
		);

		context.arc(this.x, this.y, this.rayon, 0, 2 * Math.PI, false);
		context.stroke();

		context.beginPath();
		context.font = 'bold ' + this.rayon / 4.4 + 'px Verdana, Arial, serif'; // 4.3 pour que : nom < espace dispo
		if (this.rayon <= maxImortalityRayon) {
			//Si on est invincible
			context.fillStyle = 'rgb(255, 0, 0)';
		} else if (this.skin === '../images/skin/space-yellow.png') {
			//Changement de la couleur du pseudo car illisible sur se skin
			context.fillStyle = 'rgb(0,0,0)';
		} else {
			context.fillStyle = 'rgba(255, 255, 255, 1)';
		}
		context.textAlign = 'center';
		context.fillText(this.name, this.x, this.y);
		context.stroke();
	}

	//Fonction de déplacement d'un joueur
	movePlayer(
		mouse: Mouse,
		screenHeight: number,
		screenWidth: number,
		map: Map
	): Player {
		this.distanceMousePlayerX = mouse.x - screenWidth / 2;
		this.distanceMousePlayerY = mouse.y - screenHeight / 2;
		this.angle = Math.atan2(
			//Pour savoir vers ou le joueur va avancer
			this.distanceMousePlayerY,
			this.distanceMousePlayerX
		);
		const maxMouseDistance: number = 250; //Distance joueur/souris ou plafonne la vitesse maximum du joueur
		if (this.distanceMousePlayerX > maxMouseDistance) {
			this.distanceMousePlayerX = maxMouseDistance;
		}
		if (this.distanceMousePlayerX < -maxMouseDistance) {
			this.distanceMousePlayerX = -maxMouseDistance;
		}
		if (this.distanceMousePlayerY > maxMouseDistance) {
			this.distanceMousePlayerY = maxMouseDistance;
		}
		if (this.distanceMousePlayerY < -maxMouseDistance) {
			this.distanceMousePlayerY = -maxMouseDistance;
		}

		//Changement des coordonnées du joueur
		this.x +=
			Math.cos(this.angle) *
			((Math.abs(this.distanceMousePlayerX) / this.rayon / 2) * this.speed);
		this.y +=
			Math.sin(this.angle) *
			((Math.abs(this.distanceMousePlayerY) / this.rayon / 2) * this.speed);
		this.wallsCollisionHandler(map); //appel de la verification des murs
		return this;
	}

	//Arret du joueur au niveau d'un mur
	wallsCollisionHandler(map: Map) {
		if (this.x < this.rayon) {
			this.x = this.rayon;
		}
		if (this.x > map.width - this.rayon) {
			this.x = map.width - this.rayon;
		}
		if (this.y < this.rayon) {
			this.y = this.rayon;
		}
		if (this.y > map.height - this.rayon) {
			this.y = map.height - this.rayon;
		}
	}
}

//Boucle d'affichage sur tous les joueurs
export function displayAllPlayers(
	liste: Player[],
	context: CanvasRenderingContext2D
) {
	liste.forEach(joueur => joueur.displayPlayer(context));
}

const minPourcentDiff: number = 5; //Pourcentage minimum de différence avec l'autre joueur pour le manger
const maxImortalityRayon: number = 56; //rayon maximum au dela duquel on est plus immortel

//Détection des collisions entre les joueurs
export function checkPlayerCollider(liste: Player[]) {
	for (let i = 0; i < liste.length; i++) {
		//Double boucle peut être optimiser pour diminuer les double controles
		for (let j = 0; j < liste.length; j++) {
			if (
				liste[j].id !== liste[i].id && // Pas de collisions avec sois même
				liste[j].rayon > maxImortalityRayon && //Si les 2 ne sont pas immortels
				liste[i].rayon > maxImortalityRayon
			) {
				const distance = Math.hypot(
					//distance entre les joueurs
					liste[i].x - liste[j].x,
					liste[i].y - liste[j].y
				);
				if (liste[i].rayon + liste[j].rayon > distance) {
					//si ils se touchent
					let j1Safe: number = (liste[i].rayon / 100) * minPourcentDiff; //pourcentage de difference minimum pour qu'ils puissent se manger
					let j2Safe: number = (liste[j].rayon / 100) * minPourcentDiff;
					let playerDiff: number = Math.abs(liste[i].rayon - liste[j].rayon);
					if (j2Safe < j1Safe && playerDiff > j2Safe) {
						//si le j1 est plus gros que j2 et vérifie le pourcentage minimum de différence a avoir:
						let airTotale =
							liste[j].rayon * liste[j].rayon * Math.PI +
							liste[i].rayon * liste[i].rayon * Math.PI;
						liste[i].rayon = Math.sqrt(airTotale / Math.PI); // il vole la masse de l'autre
						return [liste[j].id, liste[j].name];
					}
					if (j1Safe < j2Safe && playerDiff > j1Safe) {
						let airTotale =
							liste[j].rayon * liste[j].rayon * Math.PI +
							liste[i].rayon * liste[i].rayon * Math.PI;
						liste[j].rayon = Math.sqrt(airTotale / Math.PI);
						return [liste[i].id, liste[i].name];
					}
				}
			}
		}
	}
	return '0';
}

export function playerIsDead(liste: Player[], id: string): Player[] {
	//En cas de mort d'un joueur
	for (let i = 0; i < liste.length; i++) {
		if (liste[i].id === id) {
			liste = liste.filter(player => player.id !== id); //On enleve le joueur de la liste
		}
	}
	return liste;
}

//Calcul du poids final du joueur
export function setFinalPoids(liste: Player[], id: string): number {
	let finalPoids: number = 0;
	for (let i = 0; i < liste.length; i++) {
		if (liste[i].id === id) {
			finalPoids = liste[i].rayon * liste[i].rayon * Math.PI;
		}
	}
	return finalPoids;
}
