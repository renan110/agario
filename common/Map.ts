export default class Map {
	width: number;
	height: number;

	constructor(height: number, width: number) {
		this.width = width;
		this.height = height;
	}

	//On clear toute la map
	clearDisplay(
		context: CanvasRenderingContext2D,
		screenWidth: number,
		screenHeight: number
	) {
		context.fillStyle = 'white';
		context.fillRect(-100, -100, screenWidth * 10, screenHeight * 10);
	}

	//On dessine la map
	drawMap(context: CanvasRenderingContext2D) {
		let x = 0;
		let y = 0;

		context.lineWidth = 2;
		context.strokeStyle = 'black';
		context.beginPath();

		//Affiche la bordure:
		context.moveTo(x, 0);
		context.lineTo(0, this.width);
		context.lineTo(this.width, this.height);
		context.lineTo(this.height, 0);
		context.lineTo(0, 0);

		//Affiche le quadrillage:
		context.stroke();
		context.lineWidth = 1;
		context.globalAlpha = 0.5;
		context.beginPath();
		while (x <= this.width) {
			context.moveTo(x, 0);
			context.lineTo(x, this.width);
			x += 50;
		}
		while (y <= this.height) {
			context.moveTo(0, y);
			context.lineTo(this.height, y);
			y += 50;
		}
		context.stroke();
		context.globalAlpha = 1;
	}
}
