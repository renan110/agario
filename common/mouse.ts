export default class Mouse {
	x: number;
	y: number;
	constructor(canvas: HTMLCanvasElement) {
		this.x = 0;
		this.y = 0;
		canvas.addEventListener('mousemove', (event: MouseEvent) => {
			this.x = event.clientX - canvas.offsetLeft;
			this.y = event.clientY - canvas.offsetTop;
		});
	}
}
